#include <iostream>

#include <vector>
#include <string>
#include <string_view>
#include <sstream>
// #include <unordered_map>


namespace bwn
{

namespace detail
{

static bool streamWrite(std::ostream& _stream, const char* _bytes, size_t _count)
{
    _stream.write(_bytes, _count);
    return !_stream.fail();
}

template<typename ValueT>
static bool streamWrite(std::ostream& _stream, const ValueT& _value)
{
    return streamWrite(_stream, reinterpret_cast<const char*>(&_value), sizeof(ValueT));
}

static bool streamRead(std::istream& _stream, char* _bytes, size_t _count)
{
    _stream.read(_bytes, _count);
    return !_stream.fail();
}

template<typename ValueT>
static bool streamRead(std::istream& _stream, ValueT& _value)
{
    return streamRead(_stream, reinterpret_cast<char*>(&_value), sizeof(ValueT));
}

template<auto k_variablePtr, auto k_nameCharArray>
struct Variable;

template<typename ClassT, typename ValueT, ValueT ClassT::*k_variablePtr, const char k_nameCharArray[]>
struct Variable<k_variablePtr, k_nameCharArray>
{
    using ClassType = ClassT;
    using ValueType = ValueT;
    static constexpr ValueT ClassT::*k_variable = k_variablePtr; 
    static constexpr std::string_view k_name = std::string_view(k_nameCharArray);

    static bool serialize(std::ostream& _stream, const ClassT& _obj);
    static bool deserialize(std::istream& _stream, ClassT& _obj);

    static bool deserializeRaw(std::istream& _stream, void* _obj);
};

template<typename...>
struct VariablePack;

template<typename FirstT, typename...>
struct FirstType
{
    using Type = FirstT;
};

template<auto...k_variablePtrs, auto...k_namePtrs>
struct VariablePack<Variable<k_variablePtrs, k_namePtrs>...>
{
    using ClassType = typename FirstType<typename Variable<k_variablePtrs, k_namePtrs>::ClassType...>::Type;
    static_assert( (std::is_same_v<ClassType, typename Variable<k_variablePtrs, k_namePtrs>::ClassType> && ...), "All variables should came from the same type." );

    static bool serializeContent(std::ostream& _stream, const ClassType& _class);

    static bool deserializeContent(std::istream& _stream, ClassType& _class);
};

enum class EType : uint8_t
{
    k_bool,
    k_i32,
    k_i64,
    k_u32,
    k_u64,
    k_f32,
    k_f64,

    k_string,

    k_vector,

    k_struct,
    k_structEnd
};

template<typename ValueT>
struct ValueTrait;

template<> struct ValueTrait<bool> { static constexpr EType k_type = EType::k_bool; };
template<> struct ValueTrait<int32_t> { static constexpr EType k_type = EType::k_i32; };
template<> struct ValueTrait<int64_t> { static constexpr EType k_type = EType::k_i64; };
template<> struct ValueTrait<uint32_t> { static constexpr EType k_type = EType::k_u32; };
template<> struct ValueTrait<uint64_t> { static constexpr EType k_type = EType::k_u64; };
template<> struct ValueTrait<float> { static constexpr EType k_type = EType::k_f32; };
template<> struct ValueTrait<double> { static constexpr EType k_type = EType::k_f64; };
template<> struct ValueTrait<std::string> { static constexpr EType k_type = EType::k_string; };
template<typename ValueT> struct ValueTrait<std::vector<ValueT>> { static constexpr EType k_type = EType::k_vector; };

static bool skipCurrentValue(std::istream& _stream, EType _type);

static bool skipCurrentVariableName(std::istream& _stream)
{
    uint8_t nameSize = 0;
    if (streamRead<uint8_t>(_stream, nameSize))
    {
        return false;
    }
    _stream.ignore(nameSize);
    return !_stream.fail();
}

template<typename ValueT>
static bool skipCurrentValue(std::istream& _stream)
{
    _stream.ignore(sizeof(ValueT));
    return !_stream.fail();
}

static bool skipCurrentStringValue(std::istream& _stream)
{
    uint32_t stringSize = 0;
    if (!streamRead<uint32_t>(_stream, stringSize))
    {
        return false;
    }
    _stream.ignore(stringSize);

    return !_stream.fail();
}

static bool skipCurrentVectorValue(std::istream& _stream)
{
    uint32_t vectorSize = 0;
    if (!streamRead<uint32_t>(_stream, vectorSize))
    {
        return false;
    }

    for (uint32_t index = 0; index < vectorSize; ++index)
    {
        EType type;
        if (!streamRead<EType>(_stream, type) || !skipCurrentValue(_stream, type))
        {
            return false;
        }
    }

    return true;
}

static bool skipCurrentStructValue(std::istream& _stream)
{
    EType type = EType::k_structEnd;

    while (true)
    {
        if (!streamRead<EType>(_stream, type))
        {
            return false;
        }

        if (type == EType::k_structEnd)
        {
            break;
        }

        if (!skipCurrentVariableName(_stream) || !skipCurrentValue(_stream, type))
        {
            return false;
        }
    }
    
    return true;
}

static bool skipCurrentValue(std::istream& _stream, EType _type)
{
    switch (_type)
    {
        case EType::k_bool: return skipCurrentValue<bool>(_stream);
        case EType::k_i32: return skipCurrentValue<int32_t>(_stream);
        case EType::k_i64: return skipCurrentValue<int64_t>(_stream);
        case EType::k_u32: return skipCurrentValue<uint32_t>(_stream);
        case EType::k_u64: return skipCurrentValue<uint64_t>(_stream);
        case EType::k_f32: return skipCurrentValue<float>(_stream);
        case EType::k_f64: return skipCurrentValue<double>(_stream);
        case EType::k_string: return skipCurrentStringValue(_stream);
        case EType::k_vector: return skipCurrentVectorValue(_stream);
        case EType::k_struct: return skipCurrentStructValue(_stream);

        default: break;
    }

    return false;
}


template<typename ValueT, typename = void>
struct Serializer
{
    static constexpr EType k_type = ValueTrait<ValueT>::k_type;

    static bool serialize(std::ostream& _stream, const ValueT& _obj)
    {
        return streamWrite<uint8_t>(_stream, (uint8_t)ValueTrait<ValueT>::k_type)
            && streamWrite<ValueT>(_stream, _obj);
    }

    static bool serialize(std::ostream& _stream, const std::string_view& _name, const ValueT& _obj)
    {
        return streamWrite<uint8_t>(_stream, (uint8_t)ValueTrait<ValueT>::k_type)
            && streamWrite<uint8_t>(_stream, (uint8_t)_name.size())
            && streamWrite(_stream, _name.data(), _name.length())
            && streamWrite<ValueT>(_stream, _obj);
    }

    static bool deserializeValueOnly(std::istream& _stream, ValueT& _value)
    {
        return streamRead<ValueT>(_stream, _value);
    }
};

template<>
struct Serializer<std::string, void>
{
    static constexpr EType k_type = EType::k_string;

    static bool serialize(std::ostream& _stream, const std::string& _string)
    {
        return streamWrite<uint8_t>(_stream, (uint8_t)EType::k_string)
            && streamWrite<uint32_t>(_stream, (uint32_t)_string.length())
            && streamWrite(_stream, _string.data(), _string.length());
    }

    static bool serialize(std::ostream& _stream, const std::string_view& _name, const std::string& _string)
    {
        return streamWrite<uint8_t>(_stream, (uint8_t)EType::k_string)
            && streamWrite<uint8_t>(_stream, (uint8_t)_name.size())
            && streamWrite(_stream, _name.data(), _name.length())
            && streamWrite<uint32_t>(_stream, (uint32_t)_string.length())
            && streamWrite(_stream, _string.data(), _string.length());
    }

    static bool deserializeValueOnly(std::istream& _stream, std::string& _string)
    {
        uint32_t stringSize = 0;
        if (!streamRead<uint32_t>(_stream, stringSize))
        {
            return false;
        }

        _string.resize(stringSize);
        return streamRead(_stream, _string.data(), stringSize);
    }
};

template<typename ValueT>
struct Serializer<std::vector<ValueT>, void>
{
    static constexpr EType k_type = ValueTrait<ValueT>::k_vector;

    static bool serialize(std::ostream& _stream, const std::vector<ValueT>& _vector)
    {
        bool success = true;

        success = streamWrite<uint8_t>(_stream, (uint8_t)EType::k_vector)
            && streamWrite<uint32_t>(_stream, (uint32_t)_vector.size());

        typename std::vector<ValueT>::const_iterator endIt = _vector.end();
        for (typename std::vector<ValueT>::const_iterator it = _vector.begin(); it < endIt && success; ++it)
        {
            streamWrite<ValueT>(_stream, *it);
        }

        return success;
    }

    static bool serialize(std::ostream& _stream, const std::string_view& _name, const std::vector<ValueT>& _vector)
    {
        bool success = true;

        success = streamWrite<uint8_t>(_stream, (uint8_t)EType::k_vector)
            && streamWrite<uint8_t>(_stream, (uint8_t)_name.size())
            && streamWrite(_stream, _name.data(), _name.length())
            && streamWrite<uint32_t>(_stream, (uint32_t)_vector.size());

        typename std::vector<ValueT>::const_iterator endIt = _vector.end();
        for (typename std::vector<ValueT>::const_iterator it = _vector.begin(); it < endIt && success; ++it)
        {
            streamWrite<ValueT>(_stream, *it);
        }

        return success;
    }

    static bool deserializeValueOnly(std::istream& _stream, std::vector<ValueT>& _vector)
    {
        _vector.clear();
        uint32_t vectorSize = 0;
        if (!streamRead<uint32_t>(_stream, vectorSize))
        {
            return false;
        }

        _vector.reserve(vectorSize);
        
        for (uint32_t index = 0; index < vectorSize; ++index)
        {
            EType type;
            if (!streamRead<EType>(_stream, type))
            {
                return false;
            }

            if (type != Serializer<ValueT>::k_type)
            {
                if (!skipCurrentValue(_stream, type))
                {
                    return false;
                }
                continue;
            }

            ValueT value;
            if (!Serializer<ValueT>::deserializeValueOnly(_stream, value))
            {
                return false;
            }

            _vector.push_back(std::move(value));
        }

        return true;
    }
};

template<typename>
struct IsVariablePack : public std::false_type {};

template<typename...VariablesTs>
struct IsVariablePack<VariablePack<VariablesTs...>> : public std::true_type {};

template<typename StructT>
struct Serializer<StructT, std::enable_if_t<IsVariablePack<typename StructT::DissectVariablePack>::value>>
{
    static constexpr EType k_type = EType::k_struct;

    static bool serialize(std::ostream& _stream, const StructT& _struct)
    {
        return streamWrite<EType>(_stream, EType::k_struct)
            && StructT::DissectVariablePack::serializeContent(_stream, _struct)
            && streamWrite<EType>(_stream, EType::k_structEnd);
    }

    static bool serialize(std::ostream& _stream, const std::string_view& _name, const StructT& _struct)
    {
        return streamWrite<EType>(_stream, EType::k_struct)
            && streamWrite<uint8_t>(_stream, (uint8_t)_name.size())
            && streamWrite(_stream, _name.data(), _name.length())
            && StructT::DissectVariablePack::serializeContent(_stream, _struct)
            && streamWrite<EType>(_stream, EType::k_structEnd);
    }

    static bool deserializeValueOnly(std::istream& _stream, StructT& _struct)
    {
        return StructT::DissectVariablePack::deserializeContent(_stream, _struct);
    }
};

template<typename ClassT, typename ValueT, ValueT ClassT::*k_variablePtr, const char k_nameCharArray[]>
bool Variable<k_variablePtr, k_nameCharArray>::serialize(std::ostream& _stream, const ClassT& _class)
{
    return Serializer<ValueT>::serialize(_stream, k_name, _class.*k_variable);
}

template<typename ClassT, typename ValueT, ValueT ClassT::*k_variablePtr, const char k_nameCharArray[]>
bool Variable<k_variablePtr, k_nameCharArray>::deserialize(std::istream& _stream, ClassT& _class)
{
    return Serializer<ValueT>::deserializeValueOnly(_stream, _class.*k_variable);
}

template<auto...k_variablePtrs, auto...k_namePtrs>
bool VariablePack<Variable<k_variablePtrs, k_namePtrs>...>::serializeContent(std::ostream& _stream, const ClassType& _class)
{
    return (Variable<k_variablePtrs, k_namePtrs>::serialize(_stream, _class) && ...);
}

template<auto...k_variablePtrs, auto...k_namePtrs>
bool VariablePack<Variable<k_variablePtrs, k_namePtrs>...>::deserializeContent(std::istream& _stream, ClassType& _class)
{
    while (true)
    {
        EType type;
        if (!streamRead<EType>(_stream, type))
        {
            return false;
        }

        if (type == EType::k_structEnd)
        {
            break;
        }

        uint8_t nameSize = 0;
        if (!streamRead<uint8_t>(_stream, nameSize))
        {
            return false;
        }

        std::string variableName;
        variableName.resize(nameSize);
        if (!streamRead(_stream, variableName.data(), nameSize))
        {
            return false;
        }

        bool success = true;

        const bool found = ((Variable<k_variablePtrs, k_namePtrs>::k_name == variableName 
            && Serializer<typename Variable<k_variablePtrs, k_namePtrs>::ValueType>::k_type == type 
            && ([&_stream, &_class, &success](){ success = Variable<k_variablePtrs, k_namePtrs>::deserialize(_stream, _class); }(), true)) || ...);

        (void)(found);

        if (!success)
        {
            return false;
        }
    }

    return true;
}

} // namespace detail

template<typename StructT>
static bool serializeStruct(std::ostream& _stream, const StructT& _struct)
{
    return detail::Serializer<StructT>::serialize(_stream, _struct);
}

template<typename StructT>
static bool deserializeStruct(std::istream& _stream, StructT& _struct)
{
    detail::EType type;
    return detail::streamRead<detail::EType>(_stream, type)
        && type == detail::EType::k_struct
        && detail::Serializer<StructT>::deserializeValueOnly(_stream, _struct);
}

struct SimpleStruct
{
    int x;
    float y;

    static constexpr const char k_name_x[] = "simple_x";
    static constexpr const char k_name_y[] = "simple_y";
    using DissectVariablePack = detail::VariablePack<
        detail::Variable<&SimpleStruct::x, k_name_x>,
        detail::Variable<&SimpleStruct::y, k_name_y>>;
};

} // namespace bwn

int main(int, char*[])
{
    bwn::SimpleStruct inputStruct { 10, 20.0f };

    std::stringstream stream;

    std::cout << std::boolalpha;
    std::cout << "Serialization:   " << bwn::serializeStruct(stream, inputStruct) << '\n';
    
    stream.seekg(0);
    bwn::SimpleStruct outputStruct { 0, 0.0f };
    std::cout << "Deserialization: " << bwn::deserializeStruct(stream, outputStruct) << '\n';

    std::cout << "Output x: " << outputStruct.x << '\n';
    std::cout << "Output y: " << outputStruct.y << '\n';

    return 0;
}